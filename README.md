# Short link web application

* Run once
```bash
./build.sh
```

* Run start project
```bash
./start.sh
```

* Follow this link http://localhost/shorten-link

* Stop project
```bash
cd laradock
docker-compose down
or
docker-compose rm -sf 
```

* Manually raise project
```bash
cd laradock
docker-compose up -d nginx mysql php-fpm
```

* Work with php artisan in laradock
```bash
cd laradock
docker-compose exec php-fpm bash

For example:
php artisan make:migration create_tests_table --create=tests
```

* Work with mysql in laradock
```bash
cd laradock
docker-compose exec mysql bash

For example:
mysql -utest -ptest
```

* Logs in laradock
```bash
cd laradock
docker-compose logs nginx
docker-compose logs php-fpm
docker-compose logs mysql
```
