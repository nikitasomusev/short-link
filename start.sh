#!/usr/bin/env bash

cd laradock

docker-compose rm -sf && docker-compose up -d nginx mysql php-fpm && docker-compose ps
