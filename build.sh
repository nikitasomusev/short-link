#!/usr/bin/env bash


if [[ -f "./laradock" ]]
then
    echo "This file exists on your filesystem."
fi

git clone https://github.com/Laradock/laradock.git

cp -rf ./.env.dev ./.env
cp -rf ./.env.dev.laradock ./laradock/.env

cd laradock

docker-compose down
docker-compose up -d --build nginx mysql php-fpm
docker-compose exec php-fpm chmod -R 644 ./laradock/nginx/ssl/
sleep 30
cd ../
composer install
composer require laravel/ui
cd laradock
docker-compose exec php-fpm php artisan key:generate
docker-compose exec php-fpm php artisan migrate
docker-compose exec php-fpm php artisan storage:link
docker-compose exec php-fpm php artisan ui bootstrap && npm install && npm run dev
docker-compose down
