<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="col-12">
        <div class="col-12 text-center pt-2">
            <h2>Create short link</h2>
        </div>
        <div class="col-12">
            <form action="{{ route('generate.shorten.link.post') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        @if(isset($shortLink))
            {{ $shortLink }}
            <div class="col-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Short Link</th>
                        <th scope="col">Link</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $shortLink->id }}</td>
                        <td><a href="{{ route('shorten.link', $shortLink->code) }}" target="_blank">{{ route('shorten.link', $shortLink->code) }}</a></td>
                        <td>{{ $shortLink->link }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
