<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\ShortLink;

class ShortLinkRepository implements ShortLinkInterface
{

    /**
     * @var ShortLink
     */
    private ShortLink $shortLinkModel;

    public function __construct(ShortLink $shortLinkModel)
    {
        $this->shortLinkModel = $shortLinkModel;
    }

    public function save(string $link, string $shotLink): ShortLink
    {
        return $this->shortLinkModel->create([
            'code' => $shotLink,
            'link' => $link
        ]);
    }

    public function getCode(string $shortLink): ?ShortLink
    {
        return $this->shortLinkModel->where('code', $shortLink)->first();
    }

    public function getLink(string $link): ?ShortLink
    {
        return $this->shortLinkModel->where('link', $link)->first();
    }
}
