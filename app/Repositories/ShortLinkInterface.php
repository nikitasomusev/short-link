<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\ShortLink;

interface ShortLinkInterface
{

    /**
     * @param string $link
     * @param string $shotLink
     * @return ShortLink
     */
    public function save(string $link, string $shotLink): ShortLink;

    /**
     * @param string $shortLink
     * @return ShortLink|null
     */
    public function getCode(string $shortLink): ?ShortLink;

    /**
     * @param string $link
     * @return ShortLink|null
     */
    public function getLink(string $link): ?ShortLink;
}
