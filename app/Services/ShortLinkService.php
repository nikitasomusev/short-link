<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\ShortLink;
use App\Repositories\ShortLinkRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ShortLinkService
{

    /**
     * @var ShortLinkRepository
     */
    public ShortLinkRepository $shortLinkRepository;

    /**
     * ShortLinkService constructor.
     * @param ShortLinkRepository $shortLinkRepository
     */
    public function __construct(ShortLinkRepository $shortLinkRepository)
    {
        $this->shortLinkRepository = $shortLinkRepository;
    }

    /**
     * @param string $link
     * @return ShortLink|null
     */
    public function getOriginLink(string $link): ?ShortLink
    {
        return $this->shortLinkRepository->getLink($link);
    }

    /**
     * @param string $link
     * @return ShortLink
     */
    public function saveLink(string $link): ShortLink
    {
        $generatedShotLink = $this->generateShotLink($link);
        return $this->shortLinkRepository->save($link, $generatedShotLink);
    }

    /**
     * @param string $link
     * @return string
     */
    private function generateShotLink(string $link): string
    {
        $shortLink = substr(md5($link . mt_rand()), 0, 8);
        $foundShortLink = $this->getShortLinkByCode($shortLink);

        if ($foundShortLink) {
            $this->generateShotLink($link);
        }

        return $shortLink;
    }

    /**
     * @param string $code
     * @return ShortLink|null
     */
    public function getShortLink(string $code): ?ShortLink
    {
        $foundShortLink = $this->getShortLinkByCode($code);

        if (!$foundShortLink) {
            throw new ModelNotFoundException('Short link not found');
        }

        return $foundShortLink;
    }

    /**
     * @param string $shortLink
     * @return ShortLink|null
     */
    public function getShortLinkByCode(string $shortLink): ?ShortLink
    {
        return $this->shortLinkRepository->getCode($shortLink);
    }
}
