<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ShortLinkService;
use App\Http\Requests\StoreShortLink;

class ShortLinkController extends Controller
{

    /**
     * @var ShortLinkService
     */
    private ShortLinkService $shortLinkService;

    /**
     * ShortLinkController constructor.
     * @param ShortLinkService $shortLinkService
     */
    public function __construct(ShortLinkService $shortLinkService)
    {
        $this->shortLinkService = $shortLinkService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('link.create');
    }

    /**
     * @param StoreShortLink $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreShortLink $request)
    {
        $shortLink = $this->shortLinkService->getOriginLink($request->link);

        if (!$shortLink) {
            $shortLink = $this->shortLinkService->saveLink($request->link);
        }

        return redirect('shorten-link/' . $shortLink->code)->with('shortLink', $shortLink);
    }

    /**
     * @param string $code
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $code)
    {
        $shortLink = $this->shortLinkService->getShortLink($code);
        return view('link.show')->with('shortLink', $shortLink);
    }

    /**
     * @param string $code
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectByShortLink(string $code)
    {
        $shortLink = $this->shortLinkService->getShortLink($code);
        return redirect($shortLink->link);
    }
}
